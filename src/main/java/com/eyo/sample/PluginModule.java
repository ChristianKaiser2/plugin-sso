package com.eyo.sample;

import java.security.Key;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jose4j.keys.HmacKey;

import com.google.common.base.Charsets;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

public class PluginModule extends AbstractModule {

  private static final Logger logger = LogManager.getLogger(PluginModule.class);

  static final String PLUGIN_ID = "eyo_plugin_id";
  static final String PLUGIN_SECRET = "eyo_plugin_secret";

  @Override
  protected void configure() {
    bind(EyoSSO.class).asEagerSingleton();
  }

  @Provides
  @Singleton
  @Named(PLUGIN_ID)
  protected String providePluginID() {
    return getProperty(PLUGIN_ID);
  }

  @Provides
  @Singleton
  @Named(PLUGIN_SECRET)
  protected Key providePluginSecretKey() {
    final String secret = getProperty(PLUGIN_SECRET);
    return new HmacKey(secret.getBytes(Charsets.UTF_8));
  }

  private String getProperty(final String propName) {

    // check java opts
    String propValue = System.getProperty(propName);
    if (propValue != null) {
      logger.debug("Found '{}' in java opts.", propName);
      return propValue;
    }

    // check environmental vars
    propValue = System.getenv(propName);
    if (propValue != null) {
      logger.debug("Found '{}' in environmental variables.", propName);
      return propValue;
    }

    // fallback
    logger.warn("Unable to find '{}'. Please set it in java opts or as "
        + "environmental variable. Eyo SSO will fail.", propName);
    return propName + ".not.set";
  }
}
