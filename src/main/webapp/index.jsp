<%--@elvariable id="instanceID" type="java.lang.String"--%>
<%--@elvariable id="id" type="java.lang.String"--%>
<%--@elvariable id="externalID" type="java.lang.String"--%>
<%--@elvariable id="firstName" type="java.lang.String"--%>
<%--@elvariable id="lastName" type="java.lang.String"--%>
<%--@elvariable id="role" type="java.lang.String"--%>
<%--@elvariable id="locale" type="java.lang.String"--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

<head>
  <c:set var="url">${pageContext.request.requestURL}</c:set>
  <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

  <title>Hello Plugin</title>
  <link rel="stylesheet" href="styles.css"/>
</head>

<body>
  <table>
    <tr>
      <th>instanceID</th><td>${instanceID}</td>
    </tr>
    <tr>
      <th>id</th><td>${id}</td>
    </tr>
    <tr>
      <th>externalID</th><td>${externalID}</td>
    </tr>
    <tr>
      <th>firstName</th><td>${firstName}</td>
    </tr>
    <tr>
      <th>lastName</th><td>${lastName}</td>
    </tr>
    <tr>
      <th>role</th><td>${role}</td>
    </tr>
    <tr>
      <th>locale</th><td>${locale}</td>
    </tr>
  </table>
</body>

</html>
